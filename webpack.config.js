module.exports = {
  context: __dirname,
  entry: {
    'dr-fill': [
      './src/index'
    ]
  },
  mode: 'none',
  module: {
    rules: [{
      test: /\.jsx?$/,
      loader: 'babel-loader'
    }]
  },
  externals : {
    'lodash': 'umd lodash',
    'prop-types': 'umd prop-types',
    'react': 'umd react',
    'react-dom': 'umd react-dom'
  },
  output: {
    libraryTarget: 'umd',
    library: 'dr-fill',
    filename: '[name].js',
    path: __dirname + '/dist'
  },
  resolve: {
    extensions: ['.js', '.jsx']
  }
};
